all: migrate serve

migrate:
	python manage.py migrate

serve:
	python manage.py runserver

sniff:
	python `python -c "import os; import arpactor; BASE_DIR=os.path.dirname(os.path.abspath(arpactor.__file__)); MAIN=os.sep.join([BASE_DIR, 'main.py']); print(MAIN)"` -a ./demo_dash_actors
