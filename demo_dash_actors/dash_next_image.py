import datetime

from websocket import create_connection

from actor import Actor


class DashNextImageActor(Actor):
    MAC = "50f5da5b5937"

    def act(self):
        now = datetime.datetime.now()
        print("[%s] Dash button press registered" % now)

        ws = create_connection("ws://localhost:8000/slideshow/")
        ws.send("next")
        ws.close()
