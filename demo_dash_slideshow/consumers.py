from channels import Group

GROUPNAME = "slideshow"


def ws_connect(message):
    Group(GROUPNAME).add(message.reply_channel)


def ws_receive(message):
    if message.content['path'].strip("/") != GROUPNAME:
        return

    Group(GROUPNAME).send({
        "text": message.content["text"],
    })


def ws_disconnect(message):
    Group(GROUPNAME).discard(message.reply_channel)
